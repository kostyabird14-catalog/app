<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('categories.')->prefix('/categories')->group(function () {

    Route::name('index')->get('', [CategoryController::class, 'index']);
    Route::name('show')->get('/{id}', [CategoryController::class, 'show']);
    Route::name('store')->post('', [CategoryController::class, 'store']);
    Route::name('update')->put('/{id}', [CategoryController::class, 'update']);
    Route::name('delete')->delete('/{id}', [CategoryController::class, 'delete']);
});

Route::name('products.')->prefix('/products')->group(function () {

    Route::name('index')->get('', [ProductController::class, 'index']);
    Route::name('show')->get('/{id}', [ProductController::class, 'show']);
    Route::name('store')->post('', [ProductController::class, 'store']);
    Route::name('update')->put('/{id}', [ProductController::class, 'update']);
    Route::name('delete')->delete('/{id}', [ProductController::class, 'delete']);
});
