<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_mb_ucfirst()
    {
        $this->assertEquals('Тестовый текст', mb_ucfirst('тестовый текст'));
        $this->assertEquals('Тестовый текст', mb_ucfirst('Тестовый текст'));
        $this->assertEquals('1 тестовый текст', mb_ucfirst('1 тестовый текст'));
        $this->assertEquals('Test text', mb_ucfirst('test text'));
        $this->assertEquals('', mb_ucfirst(''));
    }
}
