<?php

namespace Tests\Feature;

use App\Models\Product;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * @return void
     */
    public function test_api_product_show()
    {
        $response = $this->getJson(route('api.products.show', ['id' => 1]));

        $response->assertOk();

        $response->assertJson([
            'success' => true,
            'data' => [
                'id' => 1,
                'title' => "Первый продукт",
                'description' => 'Описание первого продукта',
                'price' => '900.00',
                'stocks' => 5
            ]
        ]);

        $response = $this->getJson(route('api.products.show', ['id' => 4]));

        $response->assertNotFound();

        // todo
    }

    /**
     * @return void
     */
    public function test_api_product_list()
    {
        $response = $this->getJson(route('api.products.index'));

        $response->assertOk();

        $response->assertJsonCount(Product::count(), 'data');

        // todo
    }

    /**
     * @return void
     */
//    public function test_api_product_store()
//    {
//        // todo
//    }

    /**
     * @return void
     */
//    public function test_api_product_update()
//    {
//        // todo
//    }

    /**
     * @return void
     */
//    public function test_api_product_delete()
//    {
//        // todo
//    }
}
