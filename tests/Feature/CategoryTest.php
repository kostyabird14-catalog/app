<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * @return void
     */
    public function test_api_category_show()
    {
        $response = $this->getJson(route('api.categories.show', ['id' => 1]));

        $response->assertOk();

        $response->assertJson([
            'success' => true,
            'data' => [
                'id' => 1,
                'title' => 'Категория первая',
                'description'  => 'Описание первой категории'
            ]
        ]);

        $response = $this->getJson(route('api.categories.show', ['id' => 4]));

        $response->assertNotFound();

        // todo
    }

    /**
     * @return void
     */
    public function test_api_category_list()
    {
        $response = $this->get(route('api.categories.index'));

        $response->assertOk();

        // todo
    }

    /**
     * @return void
     */
//    public function test_api_category_store()
//    {
//        // todo
//    }

    /**
     * @return void
     */
//    public function test_api_category_update()
//    {
//        // todo
//    }

    /**
     * @return void
     */
//    public function test_api_category_delete()
//    {
//        // todo
//    }
}
