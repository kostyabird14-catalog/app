<?php

return [
    'class' => \App\Recipient\CurlRecipient::class,
    'url' => [
        'example' => env('RECIPIENT_URL')
    ]
];
