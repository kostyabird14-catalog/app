<?php

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 *
 */
trait ActiveTrait
{
    /**
     * @param Builder $builder
     * @param bool $value
     * @return Builder
     */
    public function scopeActive(Builder $builder, bool $value = true): Builder
    {
        return $builder->where('active', $value);
    }
}
