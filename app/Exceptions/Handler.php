<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param Request $request
     * @param Throwable $e
     * @return Response
     * @throws Throwable
     */
    public function render($request, Throwable $e): Response
    {
        if ($request->expectsJson()) {
            switch (true) {
                case $e instanceof AuthorizationException:
                    $message = $e->getMessage() ?: __('response.exception.auth');
                    $code = 401;
                    break;
                case $e instanceof ValidationException:
                    $data = $e->validator->errors()->getMessages();
                    $message = __('response.exception.validation');
                    $code = $e->status;
                    break;
                case $e instanceof NotFoundHttpException:
                    $message = strlen($e->getMessage()) ? $e->getMessage() : __('response.exception.not_found_http');
                    $code = 404;
                    break;
                case $e instanceof ModelNotFoundException:
                    $ok = $e->getMessage() && !Str::of($e->getMessage())->contains('No query results for model');
                    $message = $ok ? $e->getMessage() : __('response.exception.model_not_found');
                    $code = 404;
                    break;
                default:
                    $message = $e->getMessage() ?: __('response.exception.unknown');
                    $code = $e->getCode();
                    $code = ($code instanceof Integer && $code > 199 && $code < 600) ? $code : 400;
            }

            return response()->json([
                'data' => $data ?? null,
                'message' => strip_tags($message),
                'success' => false,
            ], $code);
        }

        return parent::render($request, $e);
    }
}
