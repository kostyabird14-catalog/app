<?php

namespace App\Recipient;

use App\Data\Result\ContentResult;
use JetBrains\PhpStorm\Pure;

final class SomeAnotherRecipient extends BaseRecipient
{
    #[Pure] public function getContent(): ContentResult
    {
        return new ContentResult('');
        // TODO: Implement getContent() method.
    }
}
