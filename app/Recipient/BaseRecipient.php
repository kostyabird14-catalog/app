<?php

namespace App\Recipient;

use App\Contracts\Recipient;

abstract class BaseRecipient implements Recipient
{
    protected ?string $url = null;

    function __construct()
    {
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }
}
