<?php

namespace App\Data\Result;

use App\Exceptions\TagCounterException;

final class TagCounterResult
{
    private array $tags = [];

    /**
     * @throws TagCounterException
     */
    public function __construct(array $data)
    {
        foreach ($data as $tag => $count) {

            if (!is_string($tag)) {
                throw new TagCounterException('Invalid tag ' . $tag);
            }

            if (!is_int($count)) {
                throw new TagCounterException('Invalid tag ' . $tag . ' value');
            }

            $this->tags[$tag] = $count;
        }
    }

    public function getTag(string $tag): int
    {
        return $this->tags[$tag] ?? 0;
    }

    public function getAll(): array
    {
        return $this->tags;
    }
}
