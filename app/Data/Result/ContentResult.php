<?php

namespace App\Data\Result;

use function App\Html\mb_strlen;

final class ContentResult
{
    public function __construct(private string $content)
    {
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getLength(): int
    {
        return mb_strlen($this->content);
    }

    public function getHtmlEntities(): string
    {
        return htmlentities($this->content);
    }
}
