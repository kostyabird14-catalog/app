<?php

namespace App\DTO\Queries;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

#[Strict]
class ProductQueryData extends DataTransferObject
{
    public ?bool $active;
    public ?string $title;
    public ?string $description;
    public ?int $price_from;
    public ?int $price_to;
    public ?int $stocks_from;
    public ?int $stocks_to;
    public bool $with_categories = false;

    /**
     * @param Request $request
     * @return static
     * @throws UnknownProperties
     */
    public static function fromRequest(Request $request): static
    {
        return new self([
            'active' => $request->has('active') ? $request->get('active') === 'y' : null,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'price_from' => $request->has('price_from') ? (int)$request->get('price_from') : null,
            'price_to' => $request->has('price_to') ? (int)$request->get('price_to') : null,
            'stocks_from' => $request->has('stocks_from') ? (int)$request->get('stocks_from') : null,
            'stocks_to' => $request->has('stocks_to') ? (int)$request->get('stocks_to') : null,
            'with_categories' => $request->get('with_categories') === 'y'
        ]);
    }
}
