<?php

namespace App\DTO\Queries;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

#[Strict]
class CategoryQueryData extends DataTransferObject
{
    public ?bool $active = null;
    public ?string $title = null;
    public ?string $description = null;
    public bool $with_products = false;

    /**
     * @throws UnknownProperties
     */
    public static function fromRequest(Request $request): CategoryQueryData
    {
        return new self([
            'active' => $request->has('active') ? $request->get('active') === 'y' : null,
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'with_products' => $request->get('with_products') === 'y'
        ]);
    }
}
