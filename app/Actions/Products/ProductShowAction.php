<?php

namespace App\Actions\Products;

use App\Actions\BaseAction;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductShowAction extends BaseAction
{
    private ?int $id = null;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(private ProductRepository $repository)
    {
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @throws ModelNotFoundException
     * @return Product|null
     */
    public function run(): ?Product
    {
        return $this->repository->find($this->id);
    }
}
