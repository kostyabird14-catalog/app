<?php

namespace App\Actions\Products;

use App\Actions\BaseAction;
use App\DTO\Queries\ProductQueryData;
use App\Repositories\ProductRepository;
use Illuminate\Support\Collection;

class ProductListAction extends BaseAction
{
    private ?ProductQueryData $filter = null;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(private ProductRepository $repository)
    {
    }

    /**
     * @param ProductQueryData $filter
     * @return $this
     */
    public function setFilter(ProductQueryData $filter): static
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return Collection
     */
    public function run(): Collection
    {
        return $this->repository->list($this->filter);
    }
}
