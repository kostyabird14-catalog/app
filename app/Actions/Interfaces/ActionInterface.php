<?php

namespace App\Actions\Interfaces;

interface ActionInterface
{
    function run();
}
