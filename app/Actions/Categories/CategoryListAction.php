<?php

namespace App\Actions\Categories;

use App\Actions\BaseAction;
use App\DTO\Queries\CategoryQueryData;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Collection;


class CategoryListAction extends BaseAction
{
    private ?CategoryQueryData $filter = null;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(private CategoryRepository $repository)
    {
    }

    /**
     * @param CategoryQueryData $filter
     * @return $this
     */
    public function setFilter(CategoryQueryData $filter): static
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @return Collection
     */
    public function run(): Collection
    {
        return $this->repository->list($this->filter);
    }
}
