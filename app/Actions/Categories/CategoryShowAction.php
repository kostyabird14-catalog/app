<?php

namespace App\Actions\Categories;

use App\Actions\BaseAction;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryShowAction extends BaseAction
{
    private ?int $id = null;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(private CategoryRepository $repository)
    {
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): static
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @throws ModelNotFoundException
     * @return Category|null
     */
    public function run(): ?Category
    {
        return $this->repository->find($this->id);
    }
}
