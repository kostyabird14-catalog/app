<?php

namespace App\Actions\Html;

use App\Actions\BaseAction;
use App\Contracts\Recipient;
use App\Exceptions\TagCounterException;
use App\Parser\TagCounterParser;

final class TagCountAction extends BaseAction
{
    private ?string $url = null;

    public function __construct(
        private Recipient        $recipient,
        private TagCounterParser $parser
    )
    {
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @throws TagCounterException
     */
    public function run(): array
    {
        if ($this->url === null) {
            throw new TagCounterException('Empty url!');
        }

        $content = $this->recipient->setUrl($this->url)->getContent();

        $result = $this->parser->setContent($content)->parse();

        return $result->getAll();
    }
}
