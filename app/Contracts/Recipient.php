<?php

namespace App\Contracts;

use App\Data\Result\ContentResult;

interface Recipient
{
    function setUrl(string $url): static;

    function getContent(): ContentResult;
}
