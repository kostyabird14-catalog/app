<?php

namespace App\Providers;

use App\Contracts\Recipient;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bind();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->macro();
    }

    private function bind()
    {
        $this->app->bind(Recipient::class, config('recipients.class'));
    }

    /**
     * @return void
     */
    private function macro()
    {
        Response::macro('success', function ($data, string $message = ''): JsonResponse {
            return response()->json([
                'data' => $data,
                'message' => trim(strip_tags($message)),
                'success' => true,
            ]);
        });

        Response::macro('fail', function (int $code = 400, string $message = '', ?array $errors = null): JsonResponse {
            return response()->json([
                'errors' => $errors,
                'message' => trim(strip_tags($message)),
                'success' => false,
            ], $code);
        });
    }
}
