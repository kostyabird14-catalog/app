<?php

namespace App\Models;

use App\Traits\Models\ActiveTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson;

/**
 * App\Models\Category
 */
class Category extends Model
{
    use HasFactory;
    use ActiveTrait;
    use HasJsonRelationships;

    protected $casts = [
        'products' => 'json',
    ];

    public function title(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => mb_convert_case(trim($value), MB_CASE_TITLE, 'UTF-8')
        );
    }

    public function description(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => mb_ucfirst(trim($value))
        );
    }

    /**
     * @return BelongsToJson
     */
    public function productsItems(): BelongsToJson
    {
        return $this->belongsToJson(Product::class, 'products');
    }
}
