<?php

namespace App\Models;

use App\Traits\Models\ActiveTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Staudenmeir\EloquentJsonRelations\Relations\HasManyJson;

/**
 * App\Models\Product
*/
class Product extends Model
{
    use HasFactory;
    use ActiveTrait;
    use HasJsonRelationships;

    public function title(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => mb_convert_case(trim($value), MB_CASE_TITLE, 'UTF-8')
        );
    }

    public function description(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => mb_ucfirst(trim($value))
        );
    }

    public function getPriceFormattedAttribute(): string
    {
        return number_format((float)$this->price, 2, '.', '');
    }

    public function categoriesItems(): HasManyJson
    {
        return $this->hasManyJson(Category::class, 'products');
    }
}
