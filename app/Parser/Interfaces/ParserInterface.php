<?php

namespace App\Parser\Interfaces;

use App\Data\Result\ContentResult;

interface ParserInterface
{
    function setContent(ContentResult $content): static;

    function parse();
}
