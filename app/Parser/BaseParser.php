<?php

namespace App\Parser;

use App\Data\Result\ContentResult;
use App\Parser\Interfaces\ParserInterface;

abstract class BaseParser implements ParserInterface
{
    protected ?ContentResult $content = null;

    public function setContent(ContentResult $content): static
    {
        $this->content = $content;

        return $this;
    }
}
