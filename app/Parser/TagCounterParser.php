<?php

namespace App\Parser;

use App\Data\Result\TagCounterResult;
use App\Exceptions\TagCounterException;

final class TagCounterParser extends BaseParser
{
    /**
     * @throws TagCounterException
     */
    public function parse(): TagCounterResult
    {
        if ($this->content === null) {
            throw new TagCounterException('Empty content');
        }

        $data = $this->parseContent();

        return new TagCounterResult($data);
    }

    private function parseContent(): array
    {
        preg_match_all('~<([^/][^>]*?)[ >]~', $this->content->getContent(), $res);

        $res = $res[1];

        return array_count_values($res);
    }
}
