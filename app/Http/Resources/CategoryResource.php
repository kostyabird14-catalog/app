<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        /** @var CategoryResource|Category $this */

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'products' => ProductResource::collection($this->whenLoaded('productsItems')),
        ];
    }
}
