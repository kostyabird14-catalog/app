<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryIndexRequest extends FormRequest
{
    /**
     * @return string[][]
     */
    public function rules(): array
    {
        return [
            'active' => ['in:y,n'],
            'title' => ['string'],
            'description' => ['string'],
            'with_products' => ['in:y,n']
        ];
    }
}
