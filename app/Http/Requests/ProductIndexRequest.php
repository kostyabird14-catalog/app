<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductIndexRequest extends FormRequest
{
    /**
     * @return string[][]
     */
    public function rules(): array
    {
        return [
            'active' => ['in:y,n'],
            'title' => ['string'],
            'description' => ['string'],
            'price_from' => ['integer'],
            'price_to' => ['integer'],
            'stocks_from' => ['integer'],
            'stocks_to' => ['integer'],
            'with_categories' => ['in:y,n'],
        ];
    }
}
