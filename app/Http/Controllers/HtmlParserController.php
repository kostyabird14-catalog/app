<?php

namespace App\Http\Controllers;

use App\Actions\Html\TagCountAction;
use App\Exceptions\TagCounterException;
use Exception;
use Illuminate\View\View;

class HtmlParserController extends Controller
{
    /**
     * @throws TagCounterException|Exception
     */
    public function index(TagCountAction $action): View
    {
        $tags = $action->setUrl(config('recipients.url.example'))->run();

        return view('html-list')->with('tags', $tags);
    }
}
