<?php

namespace App\Http\Controllers;

use App\Actions\Categories\CategoryListAction;
use App\Actions\Categories\CategoryShowAction;
use App\DTO\Queries\CategoryQueryData;
use App\Http\Requests\CategoryIndexRequest;
use App\Http\Resources\CategoryResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class CategoryController extends Controller
{
    /**
     * Список категорий продуктов
     *
     * @throws UnknownProperties
     */
    public function index(CategoryIndexRequest $request, CategoryListAction $action): JsonResponse
    {
        $categories = $action
            ->setFilter(CategoryQueryData::fromRequest($request))
            ->run();

        return response()->success(
            CategoryResource::collection($categories),
            __('response.success.categories-index')
        );
    }

    /**
     * Детальная страница категории продукта
     *
     * @throws ModelNotFoundException
     */
    public function show(int $id, CategoryShowAction $action): JsonResponse
    {
        $category = $action->setId($id)->run();

        return response()->success(
            CategoryResource::make($category),
            __('response.success.categories-show')
        );
    }

    public function store()
    {
        //todo
    }

    public function update()
    {
        //todo
    }

    public function delete()
    {
        //todo
    }
}
