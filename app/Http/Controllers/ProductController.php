<?php

namespace App\Http\Controllers;

use App\Actions\Products\ProductListAction;
use App\Actions\Products\ProductShowAction;
use App\DTO\Queries\ProductQueryData;
use App\Http\Requests\ProductIndexRequest;
use App\Http\Resources\ProductResource;
use Illuminate\Http\JsonResponse;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class ProductController extends Controller
{
    /**
     * Старница списка продуктов
     *
     * @param ProductIndexRequest $request
     * @return mixed
     * @throws UnknownProperties
     */
    public function index(ProductIndexRequest $request, ProductListAction $action): JsonResponse
    {
        $products = $action
            ->setFilter(ProductQueryData::fromRequest($request))
            ->run();

        return response()->success(
            ProductResource::collection($products),
            __('response.success.products-index')
        );
    }

    /**
     * Детальная страница товара
     *
     * @param int $id
     * @return mixed
     */
    public function show(int $id, ProductShowAction $action): JsonResponse
    {
        $category = $action->setId($id)->run();

        return response()->success(
            ProductResource::make($category),
            __('response.success.products-show')
        );
    }

    public function store()
    {
        //todo
    }

    public function update()
    {
        //todo
    }

    public function delete()
    {
        //todo
    }
}
