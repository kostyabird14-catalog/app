<?php

namespace App\Repositories;

use App\DTO\Queries\ProductQueryData;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class ProductRepository
{
    /**
     * @param Product $model
     */
    public function __construct(private Product $model)
    {
    }

    /**
     * @param ProductQueryData|null $filter
     * @return Collection|Product[]
     */
    public function list(?ProductQueryData $filter = null): Collection
    {
        return $this->getFilteredQuery($filter)->get();
    }

    /**
     * @param int $id
     * @param ProductQueryData|null $filter
     * @return Product|null
     * @throws ModelNotFoundException
     */
    public function find(int $id, ?ProductQueryData $filter = null): ?Product
    {
        return $this->getFilteredQuery($filter)->findOrFail($id);
    }

    /**
     * @param ProductQueryData|null $filter
     * @return Builder|Product
     */
    private function getFilteredQuery(?ProductQueryData $filter): Builder
    {
        $filter = $filter ?: new ProductQueryData();

        return $this->model
            ->when($filter->with_categories, fn(Builder $query) => $query->with('categoriesItems'))
            ->when(is_bool($filter->active), fn(Builder $query) => $query->active($filter->active))
            ->when(!is_null($filter->title), fn(Builder $query) => $query->where('title', 'ilike', '%' . $filter->title . '%'))
            ->when(!is_null($filter->description), fn(Builder $query) => $query->where('description', 'ilike', '%' . $filter->description . '%'))
            ->when(!is_null($filter->price_from), fn(Builder $query) => $query->where('price', '>=', $filter->price_from))
            ->when(!is_null($filter->price_to), fn(Builder $query) => $query->where('price', '<=', $filter->price_to))
            ->when(!is_null($filter->stocks_from), fn(Builder $query) => $query->where('stocks', '>=', $filter->stocks_from))
            ->when(!is_null($filter->stocks_to), fn(Builder $query) => $query->where('stocks', '<=', $filter->stocks_to));
    }
}
