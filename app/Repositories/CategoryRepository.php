<?php

namespace App\Repositories;

use App\DTO\Queries\CategoryQueryData;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class CategoryRepository
{
    /**
     * @param Category $model
     */
    public function __construct(private Category $model)
    {
    }

    /**
     * @param CategoryQueryData|null $filter
     * @return Collection|Category[]
     */
    public function list(?CategoryQueryData $filter = null): Collection
    {
        return $this->getFilteredQuery($filter)->get();
    }

    /**
     * @param int $id
     * @param CategoryQueryData|null $filter
     * @return Category|null
     * @throws ModelNotFoundException
     */
    public function find(int $id, ?CategoryQueryData $filter = null): ?Category
    {
        return $this->getFilteredQuery($filter)->findOrFail($id);
    }

    /**
     * @param CategoryQueryData|null $filter
     * @return Builder|Category
     */
    private function getFilteredQuery(?CategoryQueryData $filter): Builder
    {
        $filter = $filter ?: new CategoryQueryData();

        return $this->model
            ->when($filter->with_products, fn(Builder $query) => $query->with('productsItems'))
            ->when(is_bool($filter->active), fn(Builder $query) => $query->active($filter->active))
            ->when(!is_null($filter->title), fn(Builder $query) => $query->where('title', 'ilike', '%' . $filter->title . '%'))
            ->when(!is_null($filter->description), fn(Builder $query) => $query->where('description', 'ilike', '%' . $filter->description . '%'));
    }
}
